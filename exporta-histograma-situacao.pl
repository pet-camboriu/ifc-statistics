#! /usr/bin/env perl
# Heitor Adão Júnior <heitor.adao@gmail.com>

use v5.22;
use warnings;
use FindBin qw($Bin);
use lib $Bin , "Bin/local/lib/perl5";
use PET::Statistics qw(parseNotas);

my (@list) = @ARGV || glob('*.pdf');
if (!@list) {
	say <<'Here';
	Usage: ${__FILE__} <files-to-process.pdf (1)
	       ${__FILE__}                       (2)

Here


	#say 'Usage: ', __FILE__, ' <files-to-process.pdf> (1)';
	#say '       ', __FILE__, '                        (2)';
    exit(1);
}

my %histogram;
foreach (@list) {
    my @alunos = parseNotas($_);
    foreach my $aluno (@alunos) {
        if ($aluno =~ /.* ([[:alpha:]]+) *\Z/) {
            $histogram{"$1"}++;
        } else {
            $histogram{'exception'}++;
            say "$_;$aluno";
        }
    }
}

sub by_descending_frequency {
    $histogram{$b} <=> $histogram{$a};
}
my $somatoria;
foreach my $key (sort by_descending_frequency keys %histogram) {
    $somatoria += $histogram{$key};
    say "$key: ", $histogram{$key};
}
say '-' x 20;
say "Somatória: $somatoria";
