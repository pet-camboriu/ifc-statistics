#! /usr/bin/env perl
# Heitor Adão Júnior <heitor.adao@gmail.com>

use v5.22;
use warnings;
use FindBin qw($Bin);
use lib $Bin , "Bin/local/lib/perl5";
use PET::Statistics qw(filterByNumberAndMatricula parseNotas);


my @list = @ARGV || glob('*.pdf');
unless (@list) {
    say 'Usage: ', __FILE__, ' <files-to-process.pdf>';
    exit(1);
}

foreach (@list) {
    my @alunos = filterByNumberAndMatricula parseNotas($_);
    say "$_;", @alunos+0;
}
