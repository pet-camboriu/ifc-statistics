#! /usr/bin/env perl
# Heitor Adão Júnior <heitor.adao@gmail.com>

use v5.22;
use utf8;
use warnings;
#use Encode;
use File::Basename;
use Encode qw(decode encode);
use FindBin qw($Bin);
use lib $Bin , "Bin/local/lib/perl5";
use PET::Statistics qw(printaLinhaDoRelatorio);
use Excel::Writer::XLSX;






sub print_header {
    my $workbook = shift;
    my $curso = shift;
    my $ano = shift;
    
    my $worksheet = $workbook->add_worksheet(substr($curso, 0, 31));
    $worksheet->set_landscape();
    $worksheet->set_margins(0.5);
    $worksheet->set_paper(9); # A4
    #$worksheet->repeat_rows(4);
    $worksheet->hide_gridlines(2);
    $worksheet->fit_to_pages( 1, 0 );
    
    my $format_red = $workbook->add_format(
        border        => 1,
        center_across => 1,
        bold          => 1,
        color         => 'white',
        fg_color      => 'red',
        align         => 'vcenter',
    );

    my $format_normal = $workbook->add_format(
        border    => 1,
        align     => 'center',
        valign    => 'vcenter',
        bold      => 1,
        text_wrap => 1,
    );
    
    $worksheet->set_row(0, 37);
    $worksheet->set_column('A:A', 5);
    $worksheet->set_column('B:B', 50);
    $worksheet->set_column('C:C', 13);
    $worksheet->set_column('K:K', 12);

    $worksheet->merge_range('A1:J1', "Índice de Reprovação/Aprovação dos alunos do curso de $curso nas disciplinas curriculares - $ano", $format_red);
    my $logoPath = dirname(__FILE__) . '/pet_logo.png';
    $worksheet->write('K1', '', $format_normal);
    $worksheet->insert_image('K1', $logoPath, 8, 3, 0.1, 0.1);
    
    $worksheet->merge_range('A2:B4', 'Disciplina', $format_normal);
    $worksheet->merge_range('C2:C4', "Quantidade de\nalunos ativos", $format_normal);
    $worksheet->merge_range('D2:G3', '% de alunos com nota entre:', $format_normal);
    $worksheet->write('D4', '10 ~ 7,5', $format_normal);
    $worksheet->write('E4', '7,4 ~ 5,0', $format_normal);
    $worksheet->write('F4', '4,9 ~ 2,5', $format_normal);
    $worksheet->write('G4', '2,4 ~ 0', $format_normal);
    
    $worksheet->merge_range('H2:K2', '% de alunos', $format_normal);
    $worksheet->merge_range('H3:H4', '*Aprov', $format_normal);
    $worksheet->merge_range('I3:K3', '*Reprovados', $format_normal);
    $worksheet->write('I4', 'FS', $format_normal);
    $worksheet->write('J4', 'FI', $format_normal);
    $worksheet->write('K4', 'Total', $format_normal);
    return $worksheet;
}

sub print_content {
    my $workbook = shift;
    my @list = @_;
    
    my $worksheet = $workbook->sheets(0);
    my $format_odd = $workbook->add_format(
        border   => 1,
        fg_color => 'white',
        align    => 'center',
        valign   => 'vcenter',
    );

    my $format_even = $workbook->add_format(
        border        => 1,
        fg_color      => '#dddddd',
        align         => 'center',
        valign        => 'vcenter',
    );

    my $format_percent_odd = $workbook->add_format(
	    num_format => 0x0a,
	    border     => 1,
	    align      => 'center',
        valign     => 'vcenter',
    );

    my $format_percent_even = $workbook->add_format(
	    num_format => 0x0a,
	    border     => 1,
	    fg_color   => '#dddddd',
	    align      => 'center',
        valign     => 'vcenter',
    );
    
    my $line = 4;
    foreach my $diario_em_pdf (@list) {
        my ($curso, $ativos, $grupo1, $grupo2, $grupo3, $grupo4,
            $aprovados, $frequenciaInsuficiente,
            $frequenciaSuficiente, $reprovados) = printaLinhaDoRelatorio $diario_em_pdf;

        $worksheet->write($line,  0, $line-3, ($line % 2) ? $format_odd : $format_even);
        $worksheet->write_string($line,  1, $curso, ($line % 2) ? $format_odd : $format_even);
        
        $worksheet->write($line,  2, $ativos,                 ($line % 2) ? $format_odd         : $format_even);
        $worksheet->write($line,  3, $grupo1,                 ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line,  4, $grupo2,                 ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line,  5, $grupo3,                 ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line,  6, $grupo4,                 ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line,  7, $aprovados,              ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line,  8, $frequenciaInsuficiente, ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line,  9, $frequenciaSuficiente,   ($line % 2) ? $format_percent_odd : $format_percent_even);
        $worksheet->write($line, 10, $reprovados,             ($line % 2) ? $format_percent_odd : $format_percent_even);
        
        $line++;
    }
    return $line;
}

sub print_footer {
    my $workbook = shift;
    my $firstLine = shift;
    
    my $worksheet = $workbook->sheets(0);
    $worksheet->write($firstLine + 1, 1, '* Valores percentuais considerando as informações obtidas do Registro Escolar (SIGAA) do IFC-Camboriú.');
    $worksheet->write($firstLine + 2, 1, '* bolsistas: Gustavo e Heitor.');
    #my $date = Time::new();
    #$date->current_month("mm") . " de " . $date->currentYear("2019");
    $worksheet->write($firstLine + 2, 9, 'Data: Agosto de 2019');
}





my $curso = shift @ARGV;
if ($curso) {
    say "\$curso = $curso";
}
my $semestre = shift @ARGV;
if ($semestre) {
    say "\$semestre = $semestre";
}

my (@list) = glob('*.pdf');
#my (@list) = @ARGV || glob('*.pdf');
#if (@list < 1) {
#    die 'Usage: ', __FILE__, " <*.pdf>\n";
#}






eval {
	if (!$curso) {
		say "Parametro curso não informado, então o programa vai tentar pegar na variável de ambiente CURSO.";
		$curso = encode('UTF-8', $ENV{CURSO});
	}
	if (!$semestre) {
		say "Parametro semestre não informado, então o programa vai tentar pegar na variável de ambiente SEMESTRE.";
		$semestre = $ENV{SEMESTRE};
	}
	
	# testa se as variáveis $curso e $semestre têm algum valor 
	# testa se $curso contém acronimos a serem expandidos.
	if ($curso =~ /BSI/i) {
		$curso = "Bacharelado em Sistemas de Informação";
	} elsif ($curso =~ /TSI/i) {
		$curso = "Tecnologia em Sistemas para a Internet";
	} elsif ($curso =~ /TNI/i) {
		$curso = "Tecnologia em Negócios Imobiliários";
	} elsif ($curso =~ /LP/i) {
		$curso = "Licenciatura em Pedagogia";
	} elsif ($curso =~ /LM/i) {
		$curso = "Licenciatura em Matemática";
	}
    my $workbook = Excel::Writer::XLSX->new("$curso - $semestre.xlsx");
    $workbook->set_properties(
        title    => 'Estatística de aprovação e reprovação dos alunos do IFC Camboriú.',
        subject  => 'Relatório sobre os diários de classe dos IFC que usam o sistema SIGAA',
        author   => 'PET Camboriú',
        keywords => 'estatística, ifc, sig, aprovação, reprovação',
        comments => "Extração de dados e relatórios criados pelos bolsistas:\n"
					."Gustavo Sousa Santos e Heitor Adão Júnior.\n"
					."sob tutoria de Kleber Ersching.\n"
					."Tecnologias utilizadas: Perl, Excel::Writer::XLSX, Poppler utils (pdftotext).\n",
        #created => '2019-05-28',
    );


    my $worksheet = &print_header($workbook, $curso, $semestre);

    my $numLastLine = print_content($workbook, @list);

    print_footer($workbook, $numLastLine);

    $workbook->close();
};

if ($@) {
	say "there was an error: $@";
}



0;

__END__;



=head1
    aoeu

=description
    usage: $0
    
=examples
    use PET::Statistics ();
