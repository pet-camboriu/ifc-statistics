#! /usr/bin/env perl
# Heitor Adão Júnior <heitor.adao@gmail.com>

use v5.22;
use warnings;
use FindBin qw($Bin);
use lib $Bin , "Bin/local/lib/perl5";
use PET::Statistics qw(exportPage);


my $file;

if (@ARGV > 0) {
    $file = shift @ARGV;
} else {
    say 'usage: ' . __FILE__ . ' <diario_de_classe.pdf>';
    exit 1;
}

#my @students = parseNotas($file);
#foreach (@students) {
#    say $_;
#}

my @pag = exportPage($file, 3);
foreach (@pag) {
    say;
}
