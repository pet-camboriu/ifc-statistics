#! /usr/bin/env perl
# Heitor Adão Júnior <heitor.adao@gmail.com>

use v5.22;
use warnings;
use FindBin qw($Bin);
use lib $Bin , "Bin/local/lib/perl5";
use PET::Statistics qw(printaLinhaDoRelatorio);

my @list = @ARGV || glob('*.pdf');
unless (@list) {
    say 'Usage: ', __FILE__, ' <*.pdf>';
    exit 1;
}

foreach (@list) {
    my ($curso, $ativos, $grupo1, $grupo2, $grupo3, $grupo4,
        $aprovados, $frequenciaInsuficiente,
        $frequenciaSuficiente, $reprovados) = printaLinhaDoRelatorio $_;



    printf("%s;%d;%.2f%%;%.2f%%;%.2f%%;%.2f%%;%.2f%%;%.2f%%;%.2f%%;%.2f%%\n",
                                    $curso,
                                    $ativos,
                                    $grupo1,
                                    $grupo2,
                                    $grupo3,
                                    $grupo4,
                                    $aprovados,
                                    $frequenciaInsuficiente,
                                    $frequenciaSuficiente,
                                    $reprovados);
}
