# Declare package 'PET::Statistics'
# Heitor Adão Júnior <heitor.adao@gmail.com>

package PET::Statistics;
use v5.22;
use utf8;
use open ':encoding(UTF-8)';
use warnings;
use Exporter 'import';
our @EXPORT_OK = qw(
                     pdfinfoVersion
                     checkVersionCompatibility

                     exportPage
                     parsePage1
                     parseNotas
                     filterByStatus
                     filterByNumberAndMatricula

                     getPages

                     printaLinhaDoRelatorio
                     existeAnomaliaNaOrdemDosAlunos
                   );

sub compatibleVersions {
    return ('0.41.0' => 1,  # tested in 2019-05-13. Mint 18.3
            '0.62.0' => 1,  # tested in 2019-06. openSuse lp 15.1
            '0.77.0' => 0,  # not tested yet. released in 2019-05
                            # see the version of Poppler calling:
                            #   pdftotext -v
                            # add here the status of the version you are working
           );
}

sub popplerUtilsVersion {
    my $program = shift;
    if ( !($program eq 'pdfinfo' || $program eq 'pdftotext') ) {
        die "incorrect parameter passed to popplerUtilsVersion.\n"
           ."should be (pdfinfo|pdftotext)";
    }
    open(PDFINFO, "-|", "$program -v 2>&1");
    my @content = <PDFINFO>;
    close(PDFINFO);
    
    #$text_to_search = "example text with [foo] and more";
    #$search_string = quotemeta "[foo]";
    #print "wee" if ($text_to_search =~ /$search_string/);

    $content[0] =~ /$program version (\d+\.\d+\.\d+)/;
    return $1;
}

sub pdfinfoVersion {
    return popplerUtilsVersion('pdfinfo');
}

sub pdftotextVersion {
    return popplerUtilsVersion('pdftotext');
}

sub checkVersionCompatibility {
    my %compatibleVersions = &compatibleVersions();
    my $thisVersion = &pdftotextVersion();
    return $compatibleVersions{$thisVersion};
}

sub exportPage {
    my ($file, $page) = @_;
    my $command = "pdftotext -f $page -l $page -raw $file -";
	open(PDFTOTEXT, "-|", $command);
	my @content = <PDFTOTEXT>;
	close(PDFTOTEXT);
	chomp(@content);
	return @content;
}

sub parsePage1 {
	$_[6] =~ /Código: (.+)/;
	my $codigo = $1;

	$_[7] =~ /Disciplina: (.+)/;
	my $disciplina = $1;

	return ($codigo, $disciplina);
}

sub parsePdfInfo {
    my $filename = shift;
    my $command = "pdfinfo $filename";
    open(PDFINFO, "-|", $command);
    my @content = <PDFINFO>;
    close(PDFINFO);
    return @content;
}

sub getPages {
    my $filename = shift;
    my @content = parsePdfInfo $filename;
    $content[8] =~ /Pages: +(\d+)/;
    return $1;
}

sub filterByNumberAndMatricula {
    return grep(/\A\d+ \d{10}/, @_);
}

sub filterByStatus {
    return grep(/.+ \d{1,2}\.\d \d{1,3} [A-Z]+ */, @_);
}

sub parseNotas {
	my $file = shift;
	my $firstPage = 3;
	my @content = exportPage($file, $firstPage);
	if (! ($content[3] =~ /Lista de Notas e Faltas/) ) {
		$firstPage = 4;
		@content = exportPage($file, $firstPage);
	}

	# try the next page...
	my @contentNextPage = exportPage($file, $firstPage + 1);
	if (!($contentNextPage[0] =~ /Instituto Federal Catarinense/)) {
        push @content, @contentNextPage;
    }

	return @content;
}


sub workaroundExpandClassName {
    my $className = shift;
    if ($className eq 'SIA0350') {
        return 'UTILIZAÇÃO DE TECNOLOGIAS EMERGENTES NO DESENV...';
    } elsif ($className eq 'LPA0324') {
        return 'FUNDAMENTOS METODOLÓGICOS DAS CIÊNCIAS NATUR';
    } elsif ($className eq 'LPA0336') {
        return 'ESTÁGIO SUPERVISIONADO - MODALIDADE EM EDUCAÇÃO';
    } elsif ($className eq 'LPA0321') {
        return 'FUNDAMENTOS METODOLÓGICOS DA LÍNGUA PORT…';
    } elsif ($className eq 'LPA0331') {
        return 'ESTÁGIO SUPERVISIONADO - ANOS INICIAIS DO ENSINO F.';
    } elsif ($className eq 'LPA0348') {
        return 'TECNOLOGIAS DE INFORMAÇÃO E COMUNICAÇÃO EM ED.';
    } elsif ($className eq 'NIA0334') {
        return 'ADMINISTRAÇÃO FINANCEIRA ORÇAMENTÁRIA E CONTÁB.';
    } elsif ($className eq 'NIA0339') {
        return 'INCORPORAÇÃO, INSTITUIÇÃO DE CONDOMÍNIO E SIST.';
    } elsif ($className eq 'NIA0341') {
        return 'DESENHO ARQUITETÔNICO AUXILIADO POR COMPUT.';
    } elsif ($className eq 'NIB0325') {
        return 'DESENHO ARQUITETÔNICO AUXILIADO POR COMPUT.';
    }
    return $className;
}

sub printaLinhaDoRelatorio {
    my $file = shift;

    my $grupo1 = 0;
    my $grupo2 = 0;
    my $grupo3 = 0;
    my $grupo4 = 0;
    my $ativos = 0;
    my $aprovados = 0;
    my $frequenciaSuficiente = 0;

    my @pag1 = parsePage1(exportPage($file, 1));
    my $nomeCurso = workaroundExpandClassName($pag1[1]);

    my @students = filterByStatus parseNotas $file;
    foreach (@students) {
        if (/\(TRANCADO\)/) { next }
        /.+ (\d{1,2}\.\d) \d{1,3} ([A-Z]+)/;

        my ($notaFinal, $situacao) = ($1, $2);

        $ativos++;

        if    (defined($notaFinal) && $notaFinal > 7.5) { $grupo1++ }
        elsif (defined($notaFinal) && $notaFinal > 5)   { $grupo2++ }
        elsif (defined($notaFinal) && $notaFinal > 2.5) { $grupo3++ }
        else                                            { $grupo4++ }

        if (defined($situacao) && ($situacao =~ /REMF/ || $situacao =~ /REPF/))
           { $frequenciaSuficiente++ }

        if (defined($situacao) && $situacao =~ /APR/) { $aprovados++ }
    }

    my $percentualFrequenciaSuficiente = $frequenciaSuficiente / $ativos;
    
    my $percentualReprovados = 1-($aprovados/$ativos);

    return( $nomeCurso,
            $ativos,
            $grupo1/$ativos,
            $grupo2/$ativos,
            $grupo3/$ativos,
            $grupo4/$ativos,
            $aprovados/$ativos,
            $percentualReprovados - $percentualFrequenciaSuficiente,
            $percentualFrequenciaSuficiente,
            $percentualReprovados);
}

sub existeAnomaliaNaOrdemDosAlunos {
    my @alunos = @_;
    for (my $i = 1; $i <= @alunos; ++$i) {
        my $aluno = $alunos[$i-1];
        if ($aluno =~ /\A(\d+) \d{10}/ && $1 != $i) {
            return 1;
            #push @anomalia, $aluno;
            #say "$i -> $1";
            #say $aluno;
        }
    }

    return 0;
}

1;

