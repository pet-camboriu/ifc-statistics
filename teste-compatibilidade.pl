#! /usr/bin/env perl

use v5.22;
use FindBin qw($Bin);
use lib $Bin , "Bin/local/lib/perl5";
use PET::Statistics qw(checkVersionCompatibility pdfinfoVersion);


say "Verificando compatibilidade....", checkVersionCompatibility ? 'Ok' : 'não-compatível';
say "Versão do pdftotext: ", pdfinfoVersion;

